﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Actor : AnimatingSprite
    {
        //-----------------------
        // Behaviour
        //-----------------------

        protected Vector2 velocity = new Vector2(100,100);



        //-----------------------
        // Behaviour
        //-----------------------

        public Actor(Texture2D newTexture, int newFrameHeight, int newFrameWidth, float newFramesPerSecond) 
            : base(newTexture, newFrameHeight, newFrameWidth, newFramesPerSecond) // passes information up to the parent (base) class (Sprite)
        {

        }
        //-----------------------
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Update position based on velocity and frame time
            position += velocity * frameTime;

            //make sure it updates
            base.Update(gameTime);
        }
        //-----------------------

    }
}
