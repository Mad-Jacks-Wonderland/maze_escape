﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Maze_Escape
{
    class GameOver
    {

        //---------------------
        // Data
        //--------------------
        Text message;



        //---------------------
        // Behaviour
        //--------------------

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            // Create message
            SpriteFont tempFont = content.Load<SpriteFont>("fonts/largeFont");
            message = new Text(tempFont);
            message.SetTextString("GAME OVER");
            message.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, graphics.Viewport.Bounds.Height / 2));
            message.SetAlignment(Text.Alignment.CENTRE);
        }

        //--------------------

        public void Draw(SpriteBatch spriteBatch)
        {
            message.Draw(spriteBatch);
        }
    }
}
