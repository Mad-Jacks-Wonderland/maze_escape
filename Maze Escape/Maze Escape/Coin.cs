﻿using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Maze_Escape
{
    class Coin : Sprite
    {
        //data
        private int scoreValues = 10;

        public Coin(Texture2D newTexture)
            : base(newTexture)
        {
        }
        public int GetScore()
        {
            return scoreValues;
        }

    }
}
