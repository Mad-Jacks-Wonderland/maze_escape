﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Maze_Escape
{
    class Text
    {
        //-----------------------
        // Enums
        //-----------------------

        public enum Alignment
        {
            TOP_LEFT,
            TOP,
            TOP_RIGHT,
            LEFT,
            CENTRE,
            RIGHT,
            BOTTOM_LEFT,
            BOTTOM,
            BOTTOM_RIGHT
        }


        //--------------------
        // Data
        //--------------------

        SpriteFont font;
        string textString;
        Vector2 position;
        Color color = Color.Black;
        Alignment alignment;


        //--------------------
        // Behaviour
        //--------------------

        public Text(SpriteFont newFont)
        {
            font = newFont;
        }
        //-------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 adjustedPosition = position;

            Vector2 textSize = font.MeasureString(textString);

            switch (alignment)
            {
                case Alignment.TOP_LEFT:
                    //its done by default, no need to alter
                    break;

                case Alignment.TOP_RIGHT:
                    adjustedPosition.X -= textSize.X;
                    break;

                case Alignment.TOP:
                    adjustedPosition.X -= (textSize.X / 2);
                    break;

                case Alignment.BOTTOM_LEFT:
                    adjustedPosition.Y -= (textSize.Y);
                    break;

                case Alignment.BOTTOM_RIGHT:
                    adjustedPosition.Y -= (textSize.Y);
                    adjustedPosition.X -= (textSize.X);
                    break;

                case Alignment.BOTTOM:
                    adjustedPosition.Y -= (textSize.Y);
                    adjustedPosition.X -= (textSize.X / 2);
                    break;

                case Alignment.LEFT:
                    adjustedPosition.Y -= (textSize.Y / 2);
                    break;

                case Alignment.RIGHT:
                    adjustedPosition.Y -= (textSize.Y / 2);
                    adjustedPosition.X -= textSize.X;
                    break;

                case Alignment.CENTRE:
                    adjustedPosition.Y -= (textSize.Y / 2);
                    adjustedPosition.X -= (textSize.X / 2);
                    break;
            } 

            spriteBatch.DrawString(font, textString, adjustedPosition, color);
        }
        //-------------------------
        public void SetTextString(string newTextString)
        {
            textString = newTextString;
        }
        //-------------------------
        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }
        //-------------------------
        public void SetColor(Color newColor)
        {
            color = newColor;
        }
        //-------------------------
        public void SetAlignment(Alignment newAlignment)
        {
            alignment = newAlignment;
        }
        //-------------------------
    }
}
