﻿using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Maze_Escape
{
    class AnimatingSprite : Sprite
    {
        //-----------------------
        // Types
        //-----------------------

        struct Animation
        {
            public int startFrame;
            public int endFrame;
        }

        //-----------------------
        // Data
        //-----------------------

        // setting
        private int frameWidth;
        private int frameHeight;
        private Dictionary<string, Animation> animations = new Dictionary<string, Animation>();
        private float framesPerSecond;

        //runtime
        private int currentFrame;
        private float timeInFrame;
        private string currentAnimation;
        private bool playing = false;

        //-----------------------
        // Behaviour
        //-----------------------

        public AnimatingSprite(Texture2D newTexture , int newFrameWidth, int newFrameHeight, float newFramesPerSecond)
            : base(newTexture) // passes information up to the parent (base) class (Sprite)
        {
            frameWidth = newFrameWidth;
            frameHeight = newFrameHeight;
            framesPerSecond = newFramesPerSecond;
        }
        //-----------------------
        public void AddAnimation(string name, int startFrame, int endFrame)
        {
            Animation newAnimation = new Animation();
            newAnimation.startFrame = startFrame;
            newAnimation.endFrame = endFrame;
            animations.Add(name, newAnimation);
        }
        //-----------------------

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                //draw animating sprite

                int numColumns = texture.Width / frameWidth;
                int yFrameIndex = currentFrame / numColumns;
                int xFrameIndex = currentFrame % numColumns;

                Rectangle source = new Rectangle(xFrameIndex * frameWidth, yFrameIndex * frameHeight, frameWidth, frameHeight);

                spriteBatch.Draw(texture, position, source, Color.White);

            }
        }
        //-----------------------
        public virtual void Update(GameTime gameTime)
        {

            //dont update if we aren't playing
            if (playing == false)
                return;

            float timeSinceLastUpdate = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Add to how long wer've been in this frame
            timeInFrame += timeSinceLastUpdate;

            // Determine if it's time for a new frame
            // Calculate how long a frame should last
            float timePerFrame = 1.0f / framesPerSecond; // seconds perframe
            if(timeInFrame >= timePerFrame)
            {
                ++currentFrame;
                timeInFrame = 0;

                // Determint if we should change/reset our current animation
                Animation thisAnimation = animations[currentAnimation];

                if(currentFrame > thisAnimation.endFrame)
                {
                    //Reset our animation to the beginning 
                    currentFrame = thisAnimation.startFrame;
                }
            }

        }
        //----------------------
        public void PlayAnimation(string name)
        {

            //if animation is already running, dont do anything
            if (name == currentAnimation && playing == true) return;


            //error chacking: is this even in the dictionary?
            bool animationIsSetup = animations.ContainsKey(name);

            //Deug stops the program under certain condition
            Debug.Assert(animationIsSetup, "Animation named " + name + " not found in animating sprite.");

            //only change animation if the chosen animation is setup
            if (animationIsSetup)
            {
                //Record the name of our current animation
                currentAnimation = name;
                // Set our current frame to the first frame of our current animation
                currentFrame = animations[name].startFrame;
                // We just reset and started a new frame, so set frame tie to 0
                timeInFrame = 0;
                // Start playing the animation
                playing = true;
            }

            
        }
        //-----------------------
        public void StopAnimation()
        {
            playing = false;

            //reset ot the first frame of the animation
            currentFrame = animations[currentAnimation].startFrame;
        }
        //-----------------------

        //--------------------------
        public override Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, frameWidth, frameHeight);
        }
        //--------------------------
    }
}
