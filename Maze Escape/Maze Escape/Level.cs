﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO;

namespace Maze_Escape
{
    class Level
    {
        //---------------------
        // Data
        //--------------------
        Player player;
        Wall[,] walls;
        List<Spike> spikes = new List<Spike>();
        List<Coin> coins = new List<Coin>();
        int tileWidth;
        int tileHeight;
        int levelWidth = 100;
        int levelHeight = 100;
        Text scoreDisplay;
        Text livesDisplay;
        int currentLevel;
        bool reloadLevel = false;


        Texture2D playerSprite;
        Texture2D wallSprite;
        Texture2D coinSprite;
        Texture2D spikeSprite;
        SpriteFont UIFont;

        //---------------------
        // Behaviour
        //--------------------

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            playerSprite = content.Load<Texture2D>("graphics/PlayerAnimation");
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            coinSprite = content.Load<Texture2D>("graphics/Coin");
            spikeSprite = content.Load<Texture2D>("graphics/Spike");
            tileWidth = wallSprite.Width;
            tileHeight = wallSprite.Height;

            //Set up UI

            UIFont = content.Load<SpriteFont>("fonts/mainFont");
            scoreDisplay = new Text(UIFont);
            scoreDisplay.SetPosition(new Vector2(10, 10));

            livesDisplay = new Text(UIFont);
            livesDisplay.SetPosition(new Vector2(graphics.Viewport.Bounds.Width - 10, 10));
            livesDisplay.SetAlignment(Text.Alignment.TOP_RIGHT);


            //create player once, we will move it later
            player = new Player(playerSprite, 100, 100, 6, this);

            //TEMP! this will be moved later
            LoadLevel(1);


            

        }
        //---------------------
        private void PositionPlayer(int tileX, int tileY)
        {
           
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            player.SetPosition(tilePosition);
        }
        //----------------------------
        private void CreateWall(int tileX, int tileY)
        {


            Wall wall = new Wall(wallSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            wall.SetPosition(tilePosition);
            walls[tileX, tileY] = wall;

        }

        private void CreateSpike(int tileX, int tileY)
        {


            Spike newSpike = new Spike(spikeSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newSpike.SetPosition(tilePosition);
            spikes.Add(newSpike);

        }

        private void CreateCoin(int tileX, int tileY)
        {


            Coin newCoin = new Coin(coinSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newCoin.SetPosition(tilePosition);
            coins.Add(newCoin);

        }


        public void Update(GameTime gameTime)
        {
            player.Update(gameTime);


            //collisions with walls
            List<Wall> collidingWalls = GetTilesInBounds(player.GetBounds());
            foreach(Wall collidingWall in collidingWalls)
            {
                player.HandleCollision(collidingWall);
            }

            //collisions with coins
            foreach(Coin eachCoin in coins)
            {
                if (!reloadLevel && eachCoin.GetVisible() == true && eachCoin.GetBounds().Intersects(player.GetBounds()))
                {
                    player.HandleCollision(eachCoin);
                }
            }

            foreach (Spike eachSpike in spikes)
            {
                if (!reloadLevel && eachSpike.GetBounds().Intersects(player.GetBounds()))
                    player.HandleCollision(eachSpike);
            }
            

            scoreDisplay.SetTextString("Score: " + player.GetScore());
            livesDisplay.SetTextString("Lives: " + player.GetLives());

            //check if we need to reload the level
            if(reloadLevel == true)
            {
                LoadLevel(currentLevel);
                reloadLevel = false;
            }
        }
        public List<Wall> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Wall> tilesInBounds = new List<Wall>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Wall thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        // ------------------
        public Wall GetTile(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            // Otherwise we are within the bounds
            return walls[x, y];
        }
        //-------------
        public void Draw(SpriteBatch spriteBatch)
        {
            player.Draw(spriteBatch);

            foreach(Wall eachWall in walls)
            {
                if (eachWall != null)
                eachWall.Draw(spriteBatch);
            }

            foreach (Coin eachCoin in coins)
            {
                if (eachCoin != null)
                    eachCoin.Draw(spriteBatch);
            }

            foreach (Spike eachSpike in spikes)
            {
                if (eachSpike != null)
                    eachSpike.Draw(spriteBatch);
            }

            scoreDisplay.Draw(spriteBatch);
            livesDisplay.Draw(spriteBatch);
        }
        //---------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "Levels/level_";
            LoadLevel(baseLevelName + levelNum.ToString()+".txt");
        }
        //---------------------
        public void LoadLevel(string fileName)
        {

            //clear any existing data
            ClearLevel();

            //Create filestream to open the file 
            //and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // before we read in the individual tiles in the level, we need to know
            //how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; //eventually will be levelWidth
            int numLines = 0; //eventually will be levelHeight

            List<string> lines = new List<string>(); //This will contain all the strings of the text in the file 
            StreamReader reader = new StreamReader(fileStream); //This will let us read each line from the file
            string line = reader.ReadLine(); //Get the first line
            lineWidth = line.Length; // assume that overall width is equal to the lenght of the first line
            while(line != null) //as long as there is a line to read
            {
                lines.Add(line); //Add the current line to the list
                if (line.Length != lineWidth)
                {
                    //this means that lines have diferrent lenghts and this will prove problematic
                    throw new Exception("Lines have diferrent widths - error occured in line " + lines.Count);
                }

                //Read the next line to get ready for another step in the loop
                line = reader.ReadLine();
            }

            //We have read in all the lines of the file into our lines list
            //We can now know how many lines there were
            numLines = lines.Count;

            //now we can set up our tile array
            levelWidth = lineWidth;
            levelHeight = numLines;
            walls = new Wall[levelWidth, levelHeight];

            //Loop over every tile position and check the letter
            //there and load a tile basd on the letter
            for (int y = 0; y < levelHeight; ++y)
            {
                for (int x = 0; x < levelWidth; ++x)
                {
                    //load each tile
                    char tileType = lines[y][x];
                    LoadTile(tileType, x, y);
                }
            }

            //Verify if level is playable
            if(player == null)
            {
                throw new Exception("Level needt to have a player in order to work, add a starting point of rthe player");
            }
        }
        //---------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                case 'P':
                    PositionPlayer(tileX, tileY);
                    break;

                case 'W':
                    CreateWall(tileX, tileY);
                    break;

                case 'C':
                    CreateCoin(tileX, tileY);
                    break;

                case 'S':
                    CreateSpike(tileX, tileY);
                    break;

                case '.':
                    break; // do nothing

                default:
                    throw new Exception("level contains unsupported symbol " + tileType + " at line " + tileY + " and character " + tileX);
            }
        }
        //---------------------
        public void ResetLevel()
        {
            //delay reloading the level to AFTER the update loop
            reloadLevel = true;
          
        }
        //---------------------
        private void ClearLevel()
        {
            spikes.Clear();
            coins.Clear();
            
        }
        //---------------------
    }
}
