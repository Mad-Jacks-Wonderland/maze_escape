﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Maze_Escape
{
    class Player : Actor // player inherits from Actor (is a)
    {

        //data
        private int score = 0;
        private Level levelObject;
        private int lives = 3;


        //Constants
        private const float MOVE_ACCEL = 8000;
        private const float MOVE_DRAG_FACTOR = 0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIMATION_SPEED = 10;

        //-----------------------
        // Behaviour
        //-----------------------
        public Player(Texture2D newTexture, int newFrameHeight, int newFrameWidth, float newFramesPerSecond, Level newLevelObject)
            : base(newTexture, newFrameHeight, newFrameWidth, newFramesPerSecond)
        {

            AddAnimation("walkDown", 0, 3);
            AddAnimation("walkRight", 4, 7);
            AddAnimation("walkUp", 8, 11);
            AddAnimation("walkLeft", 12, 15);

            PlayAnimation("walkDown");

            levelObject = newLevelObject;
        }

        //-----------------------
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //get current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            //check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                PlayAnimation("walkLeft");
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
                PlayAnimation("walkRight");
            }

            if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                PlayAnimation("walkUp");
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
                PlayAnimation("walkDown");
            }

            //add the movement change to the velocity
            velocity += movementInput * MOVE_ACCEL * frameTime;

            //apply drag from the ground
            velocity *= MOVE_DRAG_FACTOR; // TODO remove magic number

            //if velocity is too high, reduce it
            if(velocity.Length() > MAX_MOVE_SPEED)
            {
                velocity.Normalize();
                velocity *= MAX_MOVE_SPEED; //TODO, remove it

            }

            //if player is not moving, stop the niamation
            if (velocity.Length() < MIN_ANIMATION_SPEED) //TODO, remove magic number
            {
                StopAnimation();
            }

            base.Update(gameTime);
            //------------------------
           
           
        }

        public void HandleCollision(Wall hitWall)
        {

            // determine collision depth and magnitude
            // this will tell us how to exit the collision if it happens
            Rectangle playerBounds = GetBounds();
            Vector2 depth = hitWall.GetCollisionDepth(playerBounds);

            //if the depth is non-zero, it means we are collising witht his wall
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                //Resolve collision along the shallow axis, as that is the
                //one we're closer to the edge on and therefore easier to "squeeze out"

                if (absDepthY < absDepthX)
                {

                    //Resolve the collision on the Y axis
                    position.Y = position.Y + depth.Y;

                }
                else
                {
                    //Resolve the collision on the X axis
                    position.X = position.X + depth.X;
                }
            }
        }
        //------------------------
        public void HandleCollision(Coin hitCoin)
        {
            //Collect coin
            //hide the coin
            hitCoin.SetVisible(false);

            //TODO:Add to score
            score += hitCoin.GetScore();
            
        }
        //------------------------
        public int GetScore()
        {
            return score;
        }
        //------------------------
        public void HandleCollision(Spike hitSpike)
        {
            //call the kill function
            Kill();
            
             

        }
        //---------------------
        private void Kill()
        {
            --lives; //subtract one life
            score = 0;

            levelObject.ResetLevel();
        }
        //---------------------
        public int GetLives()
        {
            return lives;
        }
    }
}
